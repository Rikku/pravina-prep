class Score < ActiveRecord::Base
  belongs_to :user
  belongs_to :attempt
  belongs_to :quiz
  #validates_uniqueness_of :user_id, scope: :quiz_id
end

class Question < ActiveRecord::Base
belongs_to :test
belongs_to :unit
belongs_to :category
has_many :attempts, through: :answers
has_many :options
has_many :answers
accepts_nested_attributes_for :options, :answers, :attempts

private
  def question_params
    params.require(:question).permit( :id, :test_id, :unit_id, :category_id, :test_code, :page, :number, answers_attributes: [ :id, :letter, :user_id, :question_id, :option_id, :correct ])
  end
end

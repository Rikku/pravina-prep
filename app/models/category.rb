class Category < ActiveRecord::Base
belongs_to :test
belongs_to :unit
has_many :questions
accepts_nested_attributes_for :questions
end

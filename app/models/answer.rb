class Answer < ActiveRecord::Base
  belongs_to :user
  belongs_to :attempt
  belongs_to :option
  belongs_to :question
  #validates_uniqueness_of :user_id, scope: :question_id

  after_create :characterize_answer

  def new
    @answer = Answer.new
  end

  def create
    @answer = Answer.new(params[:answer])
  end


private

  def characterize_answer
    if self.letter == self.correct_answer
    self.correct = true
    else
    self.correct = false
    end
  end

  def answer_params
    params.require(:answer).permit( :id, :letter, :user_id, :question_id, :option_id, :correct, :correct_answer )
  end
end

class User < ActiveRecord::Base
	attr_accessor :remember_token, :activation_token
  before_save do
    self.email.downcase!
    self.first_name.capitalize!
    self.last_name.capitalize!
  end
  before_create :create_activation_digest
	validates :first_name, presence: true, length: { maximum: 50 }
	validates :last_name, presence: true, length: { maximum: 50 }
	VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
	validates :email, presence: true, 
					  length: {maximum: 255}, 
					  format: { with: VALID_EMAIL_REGEX }, 
					  uniqueness: { case_sensitive: false }
	has_secure_password
	validates :password, presence: true, length: { minimum: 6 }
   has_many :attempts, dependent: :destroy
   has_many :questions, through: :answers
   has_many :answers, through: :attempts
   has_many :scores, through: :attempts
   has_many :quizzes, through: :attempts
   accepts_nested_attributes_for :attempts, :questions, :answers, :scores, :quizzes

	def self.new_token
    	SecureRandom.urlsafe_base64
  end

  def self.digest(string)
    	cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                  BCrypt::Engine.cost
    	BCrypt::Password.create(string, cost: cost)
  end

  def authenticated?(attribute, token)
    digest = send("#{attribute}_digest")
    return false if digest.nil?
    BCrypt::Password.new(digest).is_password?(token)
  end

# Forgets a user.
  def forget
    update_attribute(:remember_digest, nil)
  end

  def resend_activation
    update_activation_digest
    UserMailer.account_activation(self).deliver_now
  end

  def remember
    self.remember_token = User.new_token
    update_attribute(:remember_digest, User.digest(remember_token))
  end
  
	private

    # Creates and assigns the activation token and digest.
    def create_activation_digest
      self.activation_token  = User.new_token
      self.activation_digest = User.digest(activation_token)
    end

    def update_activation_digest
      self.activation_token  = User.new_token
      update_attribute(:activation_digest, User.digest(activation_token))
    end
end

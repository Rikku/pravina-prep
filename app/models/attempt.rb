class Attempt < ActiveRecord::Base
  belongs_to :quiz
  belongs_to :user
  has_many :answers, :dependent => :destroy
  has_many :questions, through: :answers
  has_many :scores
  #validates_uniqueness_of :user_id, scope: :quiz_id
  accepts_nested_attributes_for :questions, :answers, :scores

  def correct_answers
    return self.answers.where(:correct => true)
  end

  def incorrect_answers
    return self.answers.where(:correct => false)
  end

private
  def attempt_params
    params.require(:attempt).permit(:id, :quiz_id, :user_id, :finished, questions_attributes: [ :id, :test_id, :unit_id, :category_id, :test_code, :page, :number], answers_attributes: [ :id, :letter, :user_id, :question_id, :option_id, :correct ])
  end
end

class Test < ActiveRecord::Base
  has_many :units
  has_many :categories, through: :units
  has_many :questions, through: :categories
  accepts_nested_attributes_for :units, :categories, :questions
end

class Unit < ActiveRecord::Base
  belongs_to :test
  has_many :categories
  has_many :questions, through: :categories
  accepts_nested_attributes_for :categories, :questions
end
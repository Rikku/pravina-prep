class Quiz < ActiveRecord::Base
  has_many :attempts
  has_many :questions
  has_many :users, through: :attempts
  has_many :scores
  accepts_nested_attributes_for :questions, :users, :attempts, :scores

private

  def quiz_params
    params.require(:quiz).permit(:name, :description, questions_attributes: [ :id, :test_code, :page, :number ])
  end
  
end

class AttemptsController < ApplicationController
before_action :require_user, only: [:show, :new, :create, :index]
#before_action :load_attempt, only: [:show]


  def index
    @attempt = Attempt.all
  end

  def show
    @attempt = Attempt.find_by(id: params[:id])
    @user = current_user
    @quiz = Quiz.find_by(id: params[:quiz_id])
  end

  def new
    @user = current_user
    @quiz = Quiz.find_by(id: params[:quiz_id])
    @attempt = Attempt.new
    if @quiz.name == 'Pretest'
      @attempt.questions = Question.where(test_code: '72C').order('number')
    elsif @quiz.name == 'Mean, Median, and Mode'
      @attempt.questions = Question.where(unit_id: 2).order('number')
    elsif @quiz.name == 'Unanswered'
      #@attempt.questions = Question.includes(:answers).where.not(answers: {user_id: current_user.id  correct: true}) 
      @attempt.questions = Question.where('id NOT IN (SELECT question_id FROM answers WHERE correct = true and user_id = ?)', current_user.id).order('number')
    end
    @attempt.answers.build
    # respond_to do |format|
    #   format.html # new.html.erb
    #   format.json { redirect_to @user }
    #end
  end


  def create
    @user = current_user
    @quiz = Quiz.find_by(id: params[:quiz_id])
    @attempt = Attempt.new(attempt_params)
    #@attempt.answers = answers_attributes.map{ |a| Answer.new(a)}
    @attempt.answers.each do |a|
      if a.letter == a.correct_answer
      a.correct = true
      else
      a.correct = false
      end
    end
    if @attempt.valid? && @attempt.save
      redirect_to attempt_path(@attempt, user_id: current_user.id, quiz_id: @attempt.quiz_id)
    else
      render :new
    end
  end

private

  def load_attempt
    @attempt = Attempt.find(params[:id])
  end

  def attempt_params
    params.require(:attempt).permit(:id, :quiz_id, :user_id, :finished, questions_attributes: [ :id, :test_id, :unit_id, :category_id, :test_code, :page, :number], answers_attributes: [ :id, :letter, :user_id, :question_id, :option_id, :correct, :correct_answer ])
  end
end

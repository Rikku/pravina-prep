class QuizzesController < ApplicationController
before_action :require_user, only: [:show, :new, :create, :index]

def index
  @user = current_user
  @quizzes = Quiz.all
end

private
def quiz_params
    params.require(:quiz).permit(:id, :name, :description, user_attributes: [:id, :first_name, :last_name, :email, :password,
                                   :password_confirmation], attempts_attributes: [:id, :quiz_id, :user_id, :finished, questions_attributes: [ :id, :test_id, :unit_id, :category_id, :test_code, :page, :number], answers_attributes: [ :id, :letter, :user_id, :question_id, :option_id, :correct ]])
end

end
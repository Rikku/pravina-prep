class UsersController < ApplicationController
before_action :require_admin, only: [:index]
  def new
  if current_user
    redirect_to '/home'
  end
    @user = User.new
  end 

  def create
    @user = User.new(user_params)
      if @user.save
        UserMailer.account_activation(@user).deliver_now
        flash[:info] = " Please check your email to activate your account."
        redirect_to '/about'
      else
        # I'm gonna try to do the following another way
        #redirect_to '/signup'
        render 'new'
    end
  end 

  def require_admin 
    redirect_to '/home' unless current_user.admin? 
  end

  def index
    @users = User.all
  end

  def update
    @user = User.find_by(id: params[:id])
    redirect_to resend_path(user_id: @user.id)
  end

  def destroy
    require_admin
    User.find(params[:id]).destroy
    flash[:success] = " User deleted"
    redirect_to users_url
  end

  def show
    redirect_to '/quizzes'
  end

  def welcome
    @user = User.find_by(id: params[:user_id])
  end

  def resend
    @user = User.find_by(id: params[:user_id])
    @user.resend_activation
  end


private

    def user_params
      params.require(:user).permit(:first_name, :last_name, :email, :password,
                                   :password_confirmation, quiz_attributes: [:id, :name, :description], attempts_attributes: [:id, :quiz_id, :user_id, :finished, questions_attributes: [ :id, :test_id, :unit_id, :category_id, :test_code, :page, :number], answers_attributes: [ :id, :letter, :user_id, :question_id, :option_id, :correct ]])
    end


end

  
class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.

  #THIS IS CURRENTLY COUASING PROBLEMS WHICH NEED TO BE RESOLVED
  protect_from_forgery with: :exception
  include SessionsHelper


helper_method :current_user 



def current_user
  #begin
  #@current_user ||= User.find_by(id: session[:user_id]) if session[:user_id] 
  #rescue ActiveRecord::RecordNotFound
#end
#this is a new and updated current_user function
  if (user_id = session[:user_id])
      @current_user ||= User.find_by(id: user_id)
    elsif (user_id = cookies.signed[:user_id])
      user = User.find_by(id: user_id)
      if user && user.authenticated?(:remember, cookies[:remember_token])
        log_in user
        @current_user = user
      end
    end
end


def require_user 
  redirect_to '/signup' unless current_user 
end

def destroy 
  session[:user_id] = nil 
  redirect_to '/login', notice: "You have signed out!" 
end



end

class SessionsController < UsersController
  include SessionsHelper

  def new
    if current_user
      redirect_to '/home'
    end
  end

  def create
    @user = User.find_by_email(params[:session][:email])
    if @user && @user.authenticate(params[:session][:password])
      if @user.activated?
        log_in @user
        remember @user
        params[:session][:remember_me] == '1' ? remember(@user) : forget(@user)
        redirect_to @user
      else
        # @user.resend_activation
        # @user ||= User.find_by(id: params[:user_id])
        flash[:warning] = " Account not activated. Check your email for the activation link."
        redirect_to welcome_path(user_id: @user.id)  
      end
    else
      flash.now[:danger] = ' Invalid email/password combination'
      render 'new'
    end 
  end

  def destroy 
    log_out if logged_in?
    flash[:info] = " You have signed out!" 
    redirect_to '/about'
  end
end


class ScoresController < ApplicationController
  before_action :require_user, only: [:show, :new, :create, :index]

  def index
  end

  def show
    @user = current_user
    @score = Score.find_by(id: params[:id])
    @quiz = Quiz.find_by(id: params[:quiz_id])
    @attempt = Attempt.find_by(id: params[:id])
  end

  def create
    find_attempt
    user_score = check_score
      @user = User.find(current_user.id)
      @score = @attempt.scores.create(user_id: params["user_id"], attempt_id: params["attempt_id"], grade: user_score )
    if @score.valid? && @score.save
      redirect_to score_path(@score, user_id: current_user.id, quiz_id: @attempt.quiz_id)
    else
      render :new
    end  
  end

  def check_score
    score = 0
    @answers = []
    @attempt.answers.each do |answer|
      @answers << Answer.find(answer)
    end
    @answers.each do |answer|
      if answer.correct == true
        score = score + 1
      end
    end
    score = (score.to_f / (@attempt.answers.length)) * 100
    return score
  end

  def find_attempt
    @attempt = Attempt.find(params["attempt_id"])
  end


end

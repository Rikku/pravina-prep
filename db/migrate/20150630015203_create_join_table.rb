class CreateJoinTable < ActiveRecord::Migration
  def change
    create_join_table :attempts, :questions do |t|
      t.index [:attempt_id, :question_id]
      t.index [:question_id, :attempt_id]
    end
  end
end

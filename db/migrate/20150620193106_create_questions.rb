class CreateQuestions < ActiveRecord::Migration
  def change
    create_table :questions do |t|
      t.references :test, index: true
      t.references :unit, index: true
      t.references :category, index: true
      t.string :test_code
      t.integer :page
      t.integer :number
      end
  end
end

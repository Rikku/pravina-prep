class CreateOptions < ActiveRecord::Migration
  def change
    create_table :options do |t|
      t.references :test, index: true
      t.references :unit, index: true
      t.references :category, index: true
      t.references :question, index: true
      t.string :letter
      t.boolean :correct
    end
  end
end

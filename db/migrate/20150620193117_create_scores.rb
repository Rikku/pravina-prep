class CreateScores < ActiveRecord::Migration
  def change
    create_table :scores do |t|
      t.references :user, index: true
      t.references :attempt, index: true
      t.references :quiz, index: true
      t.integer  :grade
      t.timestamps null: false
    end
    add_index(:scores, [ :quiz_id, :user_id ], unique: true)
  end
end

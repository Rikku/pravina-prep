class RemoveIndexAnswersOnQuestionIdAndUserId < ActiveRecord::Migration
  def change
      remove_index :answers, column: [ :question_id, :user_id ]
  end
end

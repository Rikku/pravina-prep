class CreateAttempts < ActiveRecord::Migration
  def change
    create_table :attempts do |t|
      t.references :quiz, index: true
      t.references :user, index: true
      t.boolean :finished, :default => false
      t.timestamps null: false
    end
    add_index(:attempts, [:quiz_id, :user_id], unique: true)
  end
end

class CreateCategories < ActiveRecord::Migration
  def change
    create_table :categories do |t|
      t.references :test, index: true
      t.references :unit, index: true
      t.string :name
      t.string :description
    end
  end
end

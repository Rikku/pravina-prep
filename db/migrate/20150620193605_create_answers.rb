class CreateAnswers < ActiveRecord::Migration
  def change
    create_table :answers do |t|
      t.string :letter
      t.references :user, index: true
      t.references :attempt, index: true
      t.references :question, index: true
      t.references :option, index: true
      t.boolean    :correct
      t.timestamps null: false
    end
  add_index(:answers, [ :question_id, :user_id ], unique: true)

  end
end

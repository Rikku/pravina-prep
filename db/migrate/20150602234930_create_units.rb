class CreateUnits < ActiveRecord::Migration
  def change
    create_table :units do |t|
      t.references :test, index: true
      t.string :name
      t.string :description
    end
  end
end

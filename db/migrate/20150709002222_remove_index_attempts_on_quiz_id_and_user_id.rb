class RemoveIndexAttemptsOnQuizIdAndUserId < ActiveRecord::Migration
  def change
        remove_index :attempts, column: [ :quiz_id, :user_id ]

  end
end

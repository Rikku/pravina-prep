class AddIndexAnswersOnQuestionCorrectAnswer < ActiveRecord::Migration
  def change
    add_column :answers, :correct_answer, :string
  end
end

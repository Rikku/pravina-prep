# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150709202939) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "answers", force: :cascade do |t|
    t.string   "letter"
    t.integer  "user_id"
    t.integer  "attempt_id"
    t.integer  "question_id"
    t.integer  "option_id"
    t.boolean  "correct"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.string   "correct_answer"
  end

  add_index "answers", ["attempt_id"], name: "index_answers_on_attempt_id", using: :btree
  add_index "answers", ["option_id"], name: "index_answers_on_option_id", using: :btree
  add_index "answers", ["question_id"], name: "index_answers_on_question_id", using: :btree
  add_index "answers", ["user_id"], name: "index_answers_on_user_id", using: :btree

  create_table "attempts", force: :cascade do |t|
    t.integer  "quiz_id"
    t.integer  "user_id"
    t.boolean  "finished",   default: false
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  add_index "attempts", ["quiz_id"], name: "index_attempts_on_quiz_id", using: :btree
  add_index "attempts", ["user_id"], name: "index_attempts_on_user_id", using: :btree

  create_table "attempts_questions", id: false, force: :cascade do |t|
    t.integer "attempt_id",  null: false
    t.integer "question_id", null: false
  end

  create_table "categories", force: :cascade do |t|
    t.integer "test_id"
    t.integer "unit_id"
    t.string  "name"
    t.string  "description"
  end

  add_index "categories", ["test_id"], name: "index_categories_on_test_id", using: :btree
  add_index "categories", ["unit_id"], name: "index_categories_on_unit_id", using: :btree

  create_table "options", force: :cascade do |t|
    t.integer "test_id"
    t.integer "unit_id"
    t.integer "category_id"
    t.integer "question_id"
    t.string  "letter"
    t.boolean "correct"
  end

  add_index "options", ["category_id"], name: "index_options_on_category_id", using: :btree
  add_index "options", ["question_id"], name: "index_options_on_question_id", using: :btree
  add_index "options", ["test_id"], name: "index_options_on_test_id", using: :btree
  add_index "options", ["unit_id"], name: "index_options_on_unit_id", using: :btree

  create_table "questions", force: :cascade do |t|
    t.integer "test_id"
    t.integer "unit_id"
    t.integer "category_id"
    t.string  "test_code"
    t.integer "page"
    t.integer "number"
    t.string  "correct_answer"
  end

  add_index "questions", ["category_id"], name: "index_questions_on_category_id", using: :btree
  add_index "questions", ["test_id"], name: "index_questions_on_test_id", using: :btree
  add_index "questions", ["unit_id"], name: "index_questions_on_unit_id", using: :btree

  create_table "quizzes", force: :cascade do |t|
    t.string "name"
    t.string "description"
  end

  create_table "scores", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "attempt_id"
    t.integer  "quiz_id"
    t.integer  "grade"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "scores", ["attempt_id"], name: "index_scores_on_attempt_id", using: :btree
  add_index "scores", ["quiz_id", "user_id"], name: "index_scores_on_quiz_id_and_user_id", unique: true, using: :btree
  add_index "scores", ["quiz_id"], name: "index_scores_on_quiz_id", using: :btree
  add_index "scores", ["user_id"], name: "index_scores_on_user_id", using: :btree

  create_table "tests", force: :cascade do |t|
    t.string "name"
    t.string "description"
  end

  create_table "units", force: :cascade do |t|
    t.integer "test_id"
    t.string  "name"
    t.string  "description"
  end

  add_index "units", ["test_id"], name: "index_units_on_test_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "email"
    t.string   "password_digest"
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.string   "activation_digest"
    t.boolean  "activated",         default: false
    t.datetime "activated_at"
    t.boolean  "admin",             default: false
    t.string   "remember_digest"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree

end
